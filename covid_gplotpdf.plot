filename=system("echo $filename")
outputtitle=system("echo $title")

set size ratio 0.71 # for the A4 ratio
set terminal postscript enhanced color landscape
#set terminal pdfcairo enhanced color font "DejaVu Sans Mono,"
set output filename.'.ps'
set title outputtitle
set key autotitle columnhead
set datafile separator ","


## stats doesn't like time series date: must be done before set xdata time. 
stats filename u (strptime("%Y-%m-%d",strcol(1))) nooutput
set xrange[STATS_min:STATS_max]
##

set timefmt '%Y-%m-%d'
set xdata time
set format x "%d-%m\n%Y"
set xlabel "Time"
set yrange [0:]
set grid

set style line 2 linetype 1 linecolor rgb "red" linewidth 2.000
set style line 3 linetype 1 linecolor rgb "blue" linewidth 2.000
set style line 4 linetype 1 linecolor rgb "orange" linewidth 2.000
set style line 5 linetype 1 linecolor rgb "green" linewidth 2.000

set key inside top left

set ylabel "New Cases" 
plot for [i=2:2] filename u 1:i w impulses ls i t columnhead(i)
set ylabel "New Deaths" 
plot for [i=3:3] filename u 1:i  w impulses ls i t columnhead(i)
set ylabel "Total Cases" 
plot for [i=4:4] filename u 1:i w impulses ls i t columnhead(i)
set ylabel "Total Deaths" 
plot for [i=5:5] filename u 1:i w impulses ls i t columnhead(i)
