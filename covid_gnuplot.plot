filename=system("echo $filename")
outputtitle=system("echo $title")

set terminal pngcairo size width,height enhanced font 'Dejavu Sans Mono,12'
set output filename.'.png'
set title outputtitle
set datafile separator ","

set xdata time
set timefmt '%Y-%m-%d'
set format x "%d-%m\n%Y"
set xlabel "Time"
set ylabel "New Cases / New Deaths" 
set yrange [0:]
set grid

set style line 1 linetype 1 linecolor rgb "green" linewidth 1.000
set style line 2 linetype 1 linecolor rgb "red" linewidth 1.000
set style line 3 linetype 1 linecolor rgb "blue" linewidth 1.000

plot filename using 1:($2 <= 0?$2:1/0) with impulses ls 1 notitle, \
filename using 1:2 with impulses ls 2 notitle, \
filename using 1:3 with impulses ls 3 notitle
