# covid19 status for Waybar (or cli)

Waybar custom module to display covid19 case numbers (`new_cases`, `new_deaths`, ...) filtered by country.

Data is fetched from [ourworldindata.org](https://covid.ourworldindata.org/).

## Example integration in Waybar

The video shows the Waybar reporting new_cases / new_deaths (nc: X nd: Y) for a specific country. on-click (left mouse) generates a Dunst notification from latest 2 entries (past 2 days available), right-click opens a terminal and shows past 7 days.

![Video showing on-click actions](recording-covid_status-waybar.mp4)


![Waybar](waybar_screenshot.png "Waybar with Covid19 Status Indicator Custom Module")


Configuration and styling see the [dotfiles](https://gitlab.com/jbauernberger/dotfiles/-/tree/master/.config/waybar)

Data Source:

- COVID_DATA_HOST=https://covid.ourworldindata.org/
- COVID_DATA_URL=https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/owid-covid-data.json

## Installation

Dependencies:

```
## you may also need mako-notifier or dunst (include them if so):
sudo apt install notify-send jq ghostscript enscript curl
```

- jq: https://stedolan.github.io/jq/
- notify-send: assumes you also have either mako or dunst running (the -d option)
- jp: https://github.com/sgreben/jp (the `-f jp` option)
- ghostscript/enscript to create the pdf output format (the `-f pdf` option)


```
git clone https://gitlab.com/jbauernberger/covid_status.git && cd ./covid_status

# Make sure covid_status|_jp|_pdf are on your $PATH:
# e.g. to install simply symlink names in ~/bin/ to current working dir:
ln -sf ./covid_status ~/bin/covid_status
ln -sf ./covid_status_jp ~/bin/covid_status_jp
ln -sf ./covid_status_pdf ~/bin/covid_status_pdf
ln -sf ./covid_status_gnuplot ~/bin/covid_status_gnuplot
ln -sf ./covid_status_gplotpdf ~/bin/covid_status_gplotpdf
```

### Optionally (but recommended, see below note)

create symlinks to covid_status with these names on your $PATH : `covid_fetchonly`, `covid_waybar`, `covid_longformat`, `covid_dbus`:

```
for i in `echo waybar longformat fetchonly dbus`; do ln -sf ./covid_status ~/bin/covid_${i}; done

```

**Note:**

- It enables a strict check if a process named `covid_fetchonly` (from crontab) is already running and exit if so.
- It will make the log file (-L) more readable because we can define different names in waybar/config for `exec`, `on-click`, `on-click-right` targets e.g. covid_waybar, covid_dbus or covid_longformat, etc (see example waybar configuration below that uses this).
- It will be much easier to follow the rest of this documentation ;)

## Usage

```
covid_status  --help

Usage: covid_status [OPTION]

  Options:
    -h|--help               → display this help and exit
    -H|--helplong           → display this additional help and exit
    -l|--long               → past 7 days (on-click-right in Waybar)
    -d|--dunst              → past 2 days sent to notify-send (on-click in Waybar)
    -n|--notifynew          → use notify-send to alert about corpus has been refreshed
    -p|--private            → download and process in private corpus (ignore cache)
    -N|--nodownload         → do not download/update and work with local corpus only
    -u|--updatecorpus       → force update of corpus (forces a download and update of cache)
    -L|--logtofile          → output verbose/debug/error messages to file (WAYBAR_LOG=$WAYBAR_LOG)
    -v|--verbose            → output verbose messages to stderr (or to file with -L)
    -D|--debug              → output debug messages to stderr (or to file with -L)
    -F|--fetchonly          → only fetch updates (when run as a separate update job from cron)
    -c|--country <country>  → filter by country (e.g. "United Kingdom", "Italy", ...)
    -P|--plotfile <file>    → full path the plotfile (default covid_gnuplot.plot / covid_gplotpdf.plot)
    -f|--formatcmd <jp|pdf|gnuplot|gplotpdf> → pipe to external command (e.g. covid_status_jp, covid_status_pdf, covid_status_gnuplot, covid_status_gplotpdf)

```

Use `covid_status -H|less` or `--helplong` to print defaults and info about current configuration or how to change it.
e.g.:

Defaults can be changed in script:

```
    - COUNTRY_DEFAULT="United Kingdom" (-c option)
    - WAYBAR_LOG_DEFAULT="/home/foo/log/waybar/YYYYMMDD-covid_status.log"
      - When "WAYBAR_LOG" is already set in environment it is used instead
        and "WAYBAR_LOG_DEFAULT" is ignored

    - opt_filter_strip="(tests_|total_)" (see LATEST_JSON for list of keys)
      - only affects -d|-n options (avoids exceeding window size in dunst|mako)
```

Example Waybar config (filter on United Kingdom):

```
    .config/waybar/conf:
      "custom/covid": {
        "exec": "~/bin/covid_status -c 'United Kingdom' -L -v -n",
        "return-type": "json",
        "interval": 300,
        "format": "{} {icon}",
        "format-icons": [""],
        "exec-if": "ping covid.ourworldindata.org -c1",
        "on-click-right": "alacritty -e bash -i ~/bin/covid_status -c 'United Kingdom' -v -L -f gplotpdf",
        "on-click": "~/bin/covid_status -c 'United Kingdom' -v -L -d"
    }
```

If the optional symlinks mentioned in the Installation section are available, then here is an example `waybar/config` using these basenames:

```
    .config/waybar/conf:
      "custom/covid": {
        "exec": "~/bin/covid_waybar -c 'United Kingdom' -L -v -n",
        "return-type": "json",
        "interval": 300,
        "format": "{} {icon}",
        "format-icons": [""],
        "exec-if": "ping covid.ourworldindata.org -c1",
        "on-click-right": "alacritty -e bash -i ~/bin/covid_longformat -c 'United Kingdom' -v -L -l",
        "on-click": "~/bin/covid_dbus -c 'United Kingdom' -v -L -d"
    }
```

Example Sway (`.config/sway/config`) bindsym keyboard map and [jp](https://github.com/sgreben/jp) or gnuplot-pdf processing options:

```
bindsym $mod+c exec alacritty -e bash -ic "covid_status -N -L -f jp -c 'United Kingdom'"
bindsym $mod+P exec bash -ic "covid_status -N -L -f gplotpdf -c 'United Kingdom'"
```

![jp_screenshot](jp_screenshot.png "using -f jp with Sway bindsym")

Examples (command line/terminal):

```
  # long (-l) format (of last 7 days) and write verbose (-v) processing info to log (-L)
  $> covid_status -v -l -L

  # long format (sent to stdout|less), dunst format (2 days) sent to notify-send (-d)
  $> covid_status -l -d

  # filter by country (-c) using private corpus (-p) with verbose (-v) logging to file (-L)
  $> covid_status -c "United Kingdom" -p -v -L

  # update main corpus and ignore if (CHECKSUM'ed) version already present (-u), also
  # verbose (-v) processing info to logfile (-L)
  $> covid_status -u -v -L

  # write to custom logfile (-L) overriding "WAYBAR_LOG_DEFAULT" from script
  $> export WAYBAR_LOG=~/.waybar.log covid_status -v -L

  # use jp to draw charts on the terminal (launches covid_status_jp)
  $> covid_status -L -f jp -c "United Kingdom"

  # convert to pdf and show in zathura (launches covid_status_pdf)
  $> covid_status -L -f pdf -c "United Kingdom"

  # do not download from upstream, work with local data only (-N|--nodownload)
  $> covid_status -L -N -c "United Kingdom"

  # do not download from upstream, work with local data only (-N|--nodownload) and use gplotpdf output
  $> covid_status -L -N -c "United Kingdom" -f gplotpdf

```

## Prevent update of corpus (update is done by cron)

Data is only downloaded after checking if changes are available from upstream against a local cache. But without parsing the data it's impossible to tell if the changes are actually relevant for the specific country we filter for.

To prevent downloading from upstream by cli or waybar you can use `-N|--nodownload` option on the cli or in `waybar/config` which will then use whatever most recent (potentially old) locally stored data is available from a previous download. But the corpus must have been made available at least once on a previous run.

The actual update of the corpus could be done via a cron job.
E.g. here is a crontab to fetch updates every 6 hours (Must use `-F|--fetchonly` and optionally enable debug output (`-D`), logging to file (`-L`), and notify-send (`-n`) if relevant changes were found in data):

```
0 */6 * * * ~/bin/covid_fetchonly -F -L -D -n -c 'United Kingdom' &>/dev/null
```

Note, the `-F` option will also add a check to make sure no other covid_fetchonly processes are running. We should never have more than 1 update processs running since they would operate on the same corpus which could lead to corruption.

The `-n` option within crontab: this will attempt to call notify-send from the cron-job whenever a change affects our current country filter. There is no DBUS_SESSION_BUS_ADDRESS when run in cron and notify-send would fail, so -F also ensures this variable is correctly set (~/.dbus/session-bus/whatevervaue-machineID). At least when the user is logged in and has launched mako or dunst (we won't see anything in console mode obviously)

In `waybar/config` make sure the script is called with `-N` and if you want you can also remove the `exec-if` condition (the module will let you know if there is a problem with the corpus if you remove it, anyway this script handles these issues gracefully):

```
    // NOTE: using the symlink names - if you didn't install the symlinks then
    // use covid_status instead of covid_waybar, covid_longformat, covid_dbus
    "custom/covid": {
        "exec": "~/bin/covid_waybar -N -c 'United Kingdom' -L -D -N",
        "return-type": "json",
        "interval": 300,
        "format": "{} {icon}",
        "format-icons": [""],
        // "exec-if": "ping covid.ourworldindata.org -c1",
        //"on-click-right": "alacritty -e bash -i ~/bin/covid_status -c 'United Kingdom' -D -L -f gplotpdf -N",
        "on-click-right": "alacritty -e bash -i ~/bin/covid_longformat -c 'United Kingdom' -D -L -l -N",
        "on-click": "~/bin/covid_dbus -c 'United Kingdom' -D -L -d -N"
    },
```

With this setup only the cron-job is allowed to run updates of the corpus.
Also only the cron-job (when using -n) will inform user if new data is available that is relevant for their country (-c) filter.
When executed from Waybar it is guaranteed to return without any delay. (always uses cache)

## Note

- only valid json is sent to stdout (expected format by Waybar)
- verbose info or errors go to stderr (with -L also sent to `$WAYBAR_LOG`)

## Troubleshooting

- use `-D` (debug) which increases log verbosity to max
- use `-L` (log everything to `$WAYBAR_LOG`)

## Todo

- More jq filters to produce totals over date selection, or across a continent, etc.
- More gplotpdf filters to produce totals over date selection, or across a continent, etc.
- Produce better output on `-f pdf`

## Bugs

jp returns data not in order as it appears in input: https://github.com/sgreben/jp/issues/27
Use -f gplotpdf for a working pdf report (`-f pdf` just dumps plaintext into a pdf, so this needs more work)
